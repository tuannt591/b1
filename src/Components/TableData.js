import React, { Component } from 'react';
import { Table } from 'bootstrap-4-react';
import TableDataRow from './TableDataRow';

class TableData extends Component {

    renderTable = () => {
        return  this.props.dataUser.map(item => {
                    return  <TableDataRow key={item.id} dataUser={item}/>
                })
        
    }

    render() {
        
        return (
            <Table striped bordered>
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Tên</th>
                        <th>Điện thoại</th>
                        <th>Quyền</th>
                        <th>Thao tác</th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderTable()}
                </tbody>
            </Table>
        );
    }
}

export default TableData;