import React, { Component } from 'react';
import { InputGroup, Form, Button } from 'bootstrap-4-react';

class SearchForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            keyword:''
        }
    }
    
    isChange = (e) => {
        this.setState({
            keyword: e.target.value
        });
        
        
    }
    
    
    isButton = () => {
        if(this.props.showForm){
            return  <Button info outline onClick = {() => this.props.toggleForm()}>Đóng lại</Button>
        } else {
            return  <Button primary outline onClick = {() => this.props.toggleForm()}>Thêm mới</Button>
        }
    }

    render() {
        
        return (
            <div className='SearchForm'>
                <Form.Group>
                    <InputGroup>
                        <Form.Input
                            type="text"
                            placeholder="Nhập từ khóa"
                            onChange= {(e) => this.isChange(e)}
                        />
                        <InputGroup.Prepend><Button primary onClick={(val) => this.props.onSearch(this.state.keyword)}>Tìm</Button></InputGroup.Prepend>
                    </InputGroup>
                </Form.Group>
                <Form.Group>
                    {this.isButton()}
                </Form.Group>
            </div>


        );
    }
}

export default SearchForm;