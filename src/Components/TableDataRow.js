import React, { Component } from 'react';
import { Button, ButtonGroup } from 'bootstrap-4-react';
class TableDataRow extends Component {
    render() {
        return (
            <tr>
                <td>{this.props.dataUser.id}</td>
                <td>{this.props.dataUser.name}</td>
                <td>{this.props.dataUser.phone}</td>
                <td>{this.props.dataUser.permission}</td>
                <td>
                    <ButtonGroup aria-label="Basic example">
                        <Button warning>Sửa</Button>
                        <Button danger>Xóa</Button>
                    </ButtonGroup>
                </td>
            </tr>
        );
    }
}

export default TableDataRow;