import React, { Component } from 'react';
import { Card, Form, Button } from 'bootstrap-4-react';

class AddUser extends Component {

    isForm = () => {
        if(this.props.showForm){
            return  <Card>
                        <Card.Header>
                            Thêm mới user vào hệ thống
                                            </Card.Header>
                        <Card.Body>
                            <Form.Group>
                                <Form.Input type="text" placeholder="Enter user" />
                            </Form.Group>
                            <Form.Group>
                                <Form.Input type="text" placeholder="Điện thoại" />
                            </Form.Group>
                            <Form.Group>
                                <Form.Select>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </Form.Select>
                            </Form.Group>
                            <Form.Group>
                                <Button primary>Sửa</Button>
                            </Form.Group>
                        </Card.Body>
                    </Card>
        }
    }

    render() {
        return (
            <div className='AddUser'>
                {this.isForm()}
            </div>

        );
    }
}

export default AddUser;