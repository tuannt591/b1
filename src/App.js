import React, { Component } from 'react';
import './App.css';
import { Container, Row, Col } from 'bootstrap-4-react';
import SearchForm from './Components/SearchForm';
import TableData from './Components/TableData';
import AddUser from './Components/AddUser';
import dataUser from './Components/Data.json';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showForm: false,
      dataUser: dataUser,
      keyword: ''
    }
  }

  toggleForm = () => {
    this.setState({
      showForm: !this.state.showForm
    });
  }

  onSearch = (val) => {
    this.setState({
      keyword:val
    });
    console.log('--value--', this.state.keyword)
  }
  
  render() {
    const resultSearch = [];
    this.state.dataUser.forEach((item) => {
      if(item.name.toLowerCase().indexOf(this.state.keyword) !== -1){
        resultSearch.push(item);
      }
    })
    console.log('--resultSearch-----', resultSearch);
    return (
      <div className="App">
        <Container>
          <Row>
            <Col col='col lg-9'>
              <SearchForm
                showForm={this.state.showForm}
                toggleForm = {() => this.toggleForm()}
                onSearch = {(val) => this.onSearch(val)}
              />
              <TableData 
                dataUser = {resultSearch}
              />
            </Col>
            <Col col='col lg-3'>
              <AddUser
                showForm={this.state.showForm}
              />
            </Col>
          </Row>
        </Container>

      </div>
    );
  }
}

export default App;